// Code generated by protoc-gen-go. DO NOT EDIT.
// source: inquirer.proto

/*
Package pbv1 is a generated protocol buffer package.

It is generated from these files:
	inquirer.proto

It has these top-level messages:
	TelegramReply
*/
package pbv1

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type TelegramReply struct {
	Text string `protobuf:"bytes,1,opt,name=text" json:"text,omitempty"`
}

func (m *TelegramReply) Reset()                    { *m = TelegramReply{} }
func (m *TelegramReply) String() string            { return proto.CompactTextString(m) }
func (*TelegramReply) ProtoMessage()               {}
func (*TelegramReply) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

func (m *TelegramReply) GetText() string {
	if m != nil {
		return m.Text
	}
	return ""
}

func init() {
	proto.RegisterType((*TelegramReply)(nil), "v1.TelegramReply")
}

func init() { proto.RegisterFile("inquirer.proto", fileDescriptor0) }

var fileDescriptor0 = []byte{
	// 91 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0xe2, 0xcb, 0xcc, 0x2b, 0x2c,
	0xcd, 0x2c, 0x4a, 0x2d, 0xd2, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0x62, 0x2a, 0x33, 0x54, 0x52,
	0xe6, 0xe2, 0x0d, 0x49, 0xcd, 0x49, 0x4d, 0x2f, 0x4a, 0xcc, 0x0d, 0x4a, 0x2d, 0xc8, 0xa9, 0x14,
	0x12, 0xe2, 0x62, 0x29, 0x49, 0xad, 0x28, 0x91, 0x60, 0x54, 0x60, 0xd4, 0xe0, 0x0c, 0x02, 0xb3,
	0x9d, 0xd8, 0xa2, 0x58, 0x0a, 0x92, 0xca, 0x0c, 0x93, 0xd8, 0xc0, 0xfa, 0x8c, 0x01, 0x01, 0x00,
	0x00, 0xff, 0xff, 0x04, 0x3c, 0x21, 0x74, 0x49, 0x00, 0x00, 0x00,
}
